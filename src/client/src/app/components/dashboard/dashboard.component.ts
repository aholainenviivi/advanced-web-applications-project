import { Component, OnInit } from '@angular/core';
import { NgOptimizedImage } from "@angular/common";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { RouterLink } from "@angular/router";

/**
 * Component that displays the application dashboard. The dashboard includes three cards with information of routes
 * and buttons that navigate user to that route.
 */
@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [
    NgOptimizedImage,
    TranslateModule,
    RouterLink
  ],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.css'
})
export class DashboardComponent implements OnInit
{
  /**
   * Creates instance of DashboardComponent
   *
   * @constructor
   * @param {TranslateService} _translate
   */
  constructor(private _translate: TranslateService) {}

  ngOnInit()
  {
    this._translate.get("dashboard").subscribe();
  }
}
