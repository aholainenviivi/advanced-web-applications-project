import { Component, OnInit } from '@angular/core';
import { NgIf } from "@angular/common";
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from "@angular/forms";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";

import { ApiService } from "../../services/api.service";
import * as UserModel from "../../models/user.model";
import * as ApiModel from "../../models/api.model";
import { B } from "@angular/cdk/keycodes";
import { AuthService } from "../../services/auth.service";

/**
 * Component that displays the user profile and allows for updating the user profile.
 */
@Component({
  selector: 'app-profile',
  standalone: true,
  imports: [
    NgIf,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
  ],
  templateUrl: './profile.component.html',
  styleUrl: './profile.component.css'
})
export class ProfileComponent implements OnInit {
  /**
   * Is the user viewing or editing the profile.
   *
   * @protected
   * @type {"view" | "edit"}
   */
  protected profileMode: "view" | "edit" = "view";

  /**
   * The user information displayed in profile.
   *
   * @protected
   * @type {UserModel.User}
   */
  protected user!: UserModel.User;

  /**
   * The form for editing user info.
   *
   * @protected
   * @type {FormGroup}
   */
  protected profileForm!: FormGroup;

  /**
   * Has the edit form been submitted.
   *
   * @protected
   * @type {boolean}
   */
  submitted: boolean = false;

  /**
   * Was profile update successful.
   *
   * @type {boolean}
   */
  protected showSuccessAlert: boolean = false;

  constructor(private _translate: TranslateService,
              private _formBuilder: FormBuilder,
              private _router: Router,
              private _apiService: ApiService,
              private _authService: AuthService) {}

  ngOnInit()
  {
    this._initUser()
      .then(() => { this._initForm(); });
    this._translate.get("profile").subscribe();
  }

  /**
   * Checks if password and password validation match
   *
   * @method
   * @protected
   * @returns {void}
   */
  protected passwordMatchValidator(): void
  {
    // TODO make this a custom validator
    // The input element updates it's class before this runs -> password confirmation required error doesn't show until
    // second input event of password input + error is displayed in console
    // This would be fixed with custom validator but the custom validators do not work in register form and I'd assume
    // the same problems remain here too

    // no password change
    if (this.profileForm.value.password === "")
    {
      this.profileForm.controls["passwordConfirmation"].setValidators(null);
      this.profileForm.controls["passwordConfirmation"].setValue("");
    }
    // password changed, password confirmation empty
    else if (this.profileForm.value.passwordConfirmation === "")
    {
      this.profileForm.controls["passwordConfirmation"].setValidators(Validators.required);
    }
    // password changed, password confirmation does not match
    else if (this.profileForm.value.passwordConfirmation.trim() !== this.profileForm.value.password.trim())
    {
      this.profileForm.controls["passwordConfirmation"].setErrors({ mismatch: true });
    }

    this.profileForm.updateValueAndValidity();
  }

  /**
   * Displays the edit mode of profile.
   *
   * @method
   * @protected
   * @returns {void}
   */
  protected toggleEditMode(): void
  {
    this.profileMode = "edit";
  }

  /**
   * Submits the profile update.
   *
   * @method
   * @protected
   * @returns {void}
   */
  protected onSubmit(): void
  {
    this.submitted = true;
    if (this.profileForm.invalid) return;

    this._saveProfile();
  }

  /**
   * Cancels editing of profile.
   *
   * @method
   * @protected
   * @returns {void}
   */
  protected onCancel(): void
  {
    this._resetForm();
    this.profileMode = "view";
  }

  /**
   *
   *
   * @method
   * @protected
   * @returns {Promise<void>}
   */
  protected async deleteUser(): Promise<void>
  {
    // TODO ask for confirmation before deleting user
    this._apiService.deleteUser().subscribe(async (result) => {
      if (!result.success)
      {
        console.error("An error occurred.");
      }
      else
      {
        this._authService.deleteToken();
        await this._router.navigate(["login"]);
      }
    });
  }

  /**
   * Fetches the user profile.
   *
   * @method
   * @private
   * @returns {Promise<void>}
   */
  private _initUser = (): Promise<void> => {
    return new Promise<void>((resolve) => {
      this._apiService.getUserProfile().subscribe((response: ApiModel.Response) => {
        // response has been sent from server as json object -> date object has been converted to string
        // convert the string back to date object
        // typescript thinks the date is a date object so some conversions need to be done for trickery
        const user: UserModel.User = response.value as UserModel.User;
        user.registeredAt = new Date(user.registeredAt as unknown as string);
        this.user = user;
        resolve();
      });
    });
  }

  /**
   * Initializes profile edit form.
   *
   * @method
   * @private
   * @returns {void}
   */
  private _initForm = (): void => {
    this.profileForm = this._formBuilder.group({
        "firstName": new FormControl(this.user.firstName || "", Validators.pattern(new RegExp("^(?!\\s*$).+"))),
        "lastName": new FormControl(this.user.lastName || "", Validators.pattern(new RegExp("^(?!\\s*$).+"))),
        "username": new FormControl(this.user.username, [
          Validators.required,
          Validators.pattern(new RegExp("^(?!\\s*$).+"))
        ]),
        "bio": new FormControl(this.user.bio || "", [
          Validators.maxLength(1000),
          Validators.pattern(new RegExp("^(?!\\s*$).+")),
        ]),
        "email": new FormControl(this.user.email || "", [
          Validators.email,
          Validators.pattern(new RegExp("^(?!\\s*$).+"))
        ]),
        "password": new FormControl("", Validators.pattern(new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#$%^&*()_+{}:<>?,./;'[\\]\\\\~-])(.{8,})$"))),
        "passwordConfirmation": new FormControl("")
    });
  }

  /**
   * Handles successful profile update.
   *
   * @method
   * @private
   * @returns {void}
   */
  private _handleSuccess = (): void => {
    this._initUser()
      .then(() => {
        this.profileMode = "view";
        this.showSuccessAlert = true;
        setTimeout(() => {
          this.showSuccessAlert = false;
        }, 5000);
      });
  }

  /**
   * Handles errors in profile update.
   *
   * @method
   * @private
   * @returns {void}
   */
  private _handleError = (error: ApiModel.Error): void => {
    switch (error.type)
    {
      case "duplicate key":
        (this.profileForm.get(error.value as string) as FormControl).setErrors({ 'duplicate': true });
        break;

      case "invalid field":
        console.error("Invalid field(s) in request");
        for (let value of error.values as string[])
        {
          (this.profileForm.get(error[value] as string) as FormControl).setErrors({ 'invalid': true });
        }
        break;

      case "unknown":
        console.error("Unknown error")
        break;

      default:
        console.error(error)
        break;
    }
  }

  /**
   * Handles response from server resulted in profile update.
   *
   * @method
   * @private
   * @returns {void}
   */
  private _handleResponse = (response: ApiModel.Response): void => {
    if (response.success)
    {
      this._handleSuccess();
    }
    else
    {
      this._handleError(response.error as ApiModel.Error);
    }
  }

  /**
   * Saves the edited user profile.
   *
   * @method
   * @private
   * @returns {void}
   */
  private _saveProfile(): void
  {
    const updatedUser: UserModel.User = {
      firstName: this.profileForm.value["firstName"] === "" ? undefined : this.profileForm.value["firstName"],
      lastName: this.profileForm.value["lastName"] === "" ? undefined : this.profileForm.value["lastName"],
      username: this.profileForm.value["username"],
      bio: this.profileForm.value["bio"] === "" ? undefined : this.profileForm.value["bio"],
      email: this.profileForm.value["email"] === "" ? undefined : this.profileForm.value["email"],
      password: this.profileForm.value["password"] === "" ? undefined : this.profileForm.value["password"]
    }

    this._apiService.updateUserProfile(updatedUser).subscribe((response: ApiModel.Response) => {
      this._handleResponse(response);
    });
  }

  /**
   * Resets the profile edit form.
   *
   * @method
   * @private
   * @returns {void}
   */
  private _resetForm(): void
  {
    this.profileForm.reset(this.user);
    this._initForm();
  }
}
