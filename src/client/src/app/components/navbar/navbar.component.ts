import { Component, OnInit } from '@angular/core';
import { NgIf } from "@angular/common";
import { RouterLink, RouterLinkActive } from "@angular/router";
import { NgbDropdown, NgbDropdownItem, NgbDropdownMenu, NgbDropdownToggle } from "@ng-bootstrap/ng-bootstrap";
import { TranslateModule, TranslateService } from "@ngx-translate/core";

import { AuthService } from "../../services/auth.service";

/**
 * Component that displays the application navbar.
 */
@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [
    NgIf,
    RouterLinkActive,
    RouterLink,
    NgbDropdown,
    NgbDropdownToggle,
    NgbDropdownMenu,
    NgbDropdownItem,
    TranslateModule
  ],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent implements OnInit
{
  title: string = "Bootleg Tinder";
  isCollapsed: boolean = true;

  constructor(private _translate: TranslateService,
              private _authService: AuthService) {}

  ngOnInit()
  {
    this._translate.get('navbar').subscribe();
  }

  /**
   * Checks if user is logged in.
   *
   * @method
   * @protected
   * @returns <boolean>
   */
  protected isLoggedIn(): boolean
  {
    return this._authService.isLoggedIn();
  }

  /**
   * Checks if user is logged out.
   *
   * @method
   * @protected
   * @returns <boolean>
   */
  isLoggedOut(): boolean
  {
    return !this._authService.isLoggedIn();
  }

  /**
   * Toggles if the navbar is collapsed or not.
   *
   * @method
   * @protected
   * @returns {void}
   */
  protected toggleNavbar(): void
  {
    this.isCollapsed = !this.isCollapsed;
  }

  /**
   * Logs user out.
   *
   * @method
   * @protected
   * @returns {Promise<boolean>}
   */
  protected onLogOut(): Promise<boolean>
  {
    // TODO add confirmation for log out
    return this._authService.logout();
  }

  /**
   * Changes the application language.
   *
   * @method
   * @protected
   * @param {MouseEvent} event
   * @param {string} lang
   * @returns {void}
   */
  protected changeLanguage(event: MouseEvent, lang: string): void
  {
    event.preventDefault();
    this._translate.use(lang);
  }
}
