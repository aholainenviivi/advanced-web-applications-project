import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  ReactiveFormsModule,
  Validators, ValidatorFn, ValidationErrors
} from "@angular/forms";
import { NgIf, NgOptimizedImage } from "@angular/common";
import { MatDialog } from "@angular/material/dialog";
import { Router, RouterLink } from "@angular/router";

import { TermsAndConditionsDialogComponent } from "../terms-and-conditions-dialog/terms-and-conditions-dialog.component";
import { NewUser } from "../../models/user.model";
import { ApiService } from "../../services/api.service";
import * as ApiModel from "../../models/api.model";
import { TranslateModule, TranslateService } from "@ngx-translate/core";

/**
 * Component for displaying registering page of new users.
 */
@Component({
  selector: 'app-register',
  standalone: true,
  imports: [
    NgOptimizedImage,
    ReactiveFormsModule,
    NgIf,
    TranslateModule,
    RouterLink
  ],
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit
{
  /**
   * Has register form been submitted.
   *
   * @protected
   * @type {boolean}
   */
  protected submitted: boolean = false;

  /**
   * Do the password and password confirmation match.
   *
   * @protected
   * @type {boolean}
   */
  protected passwordsMatch: boolean = false;

  constructor(private _translate: TranslateService,
              private _router: Router,
              private _dialog: MatDialog,
              private _apiService: ApiService) {}

  ngOnInit()
  {
    this._translate.get("register").subscribe();
  }

  /**
   * Reactive form for registering new user.
   *
   * @type {FormGroup}
   */
  protected registerForm: FormGroup = new FormGroup({
    firstName: new FormControl("", Validators.pattern(new RegExp("^(?!\\s*$).+"))),
    lastName: new FormControl("", Validators.pattern(new RegExp("^(?!\\s*$).+"))),
    username: new FormControl("", [
      Validators.required,
      Validators.pattern(new RegExp("^(?!\\s*$).+"))
    ]),
    email: new FormControl("", [
      Validators.email,
      Validators.pattern(new RegExp("^(?!\\s*$).+"))
    ]),
    password: new FormControl("", [
      Validators.required,
      Validators.pattern(new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#$%^&*()_+{}:<>?,./;'[\\]\\\\~-])(.{8,})$"))
    ]),
    passwordConfirmation: new FormControl("", Validators.required),
    agreedToTermsAndConditions: new FormControl(false, Validators.requiredTrue)
  },
    // { validators: this.passwordMatchValidator } // not working
  );

  // TODO get group level validator for password match working
  // passwordMatchValidator(control: AbstractControl): ValidatorFn {
  //   // Validator function never gets called for some reason
  //   return (control: AbstractControl): ValidationErrors | null => {
  //     return (control.get("password") as FormControl).value === (control.get("passwordConfirmation") as FormControl).value
  //       ? null : { "mismatch": true };
  //   }
  // }


  /**
   * Checks if password and password confirmation match.
   *
   * @method
   * @protected
   * @returns {void}
   */
  protected passwordMatchValidator(): void
  {
    if (this.registerForm.controls['password'].value === this.registerForm.controls['passwordConfirmation'].value || this.registerForm.controls['passwordConfirmation'].value === "")
    {
      this.registerForm.controls['passwordConfirmation'].setErrors(null);
      if (this.registerForm.controls['passwordConfirmation'].value === "")
      {
        this.registerForm.controls['passwordConfirmation'].setErrors({ required: true });
      }
    }
    else
    {
      this.registerForm.controls['passwordConfirmation'].setErrors({ mismatch: this.registerForm.controls['password'].value !== this.registerForm.controls['passwordConfirmation'].value });
    }
    this.registerForm.updateValueAndValidity();
  }

  /**
   * Toggles modal to show terms and conditions.
   *
   * @method
   * @protected
   * @returns {void}
   */
  protected showTermsAndConditions(): boolean
  {
    this._dialog.open(TermsAndConditionsDialogComponent)
    return false;
  }

  /**
   * Submits the register form.
   *
   * @method
   * @protected
   * @returns {void}
   */
  protected onSubmit(): void
  {
    this.submitted = true;

    if (this.registerForm.invalid && !this.passwordsMatch) return;

    this._registerUser();
  }

  /**
   * Resets the register form.
   *
   * @method
   * @protected
   * @returns {void}
   */
  protected onReset(): void
  {
    this.registerForm.reset();
    this.submitted = false;
  }

  /**
   * Handles error received from server after submitting register request.
   *
   * @method
   * @private
   * @returns {void}
   */
  private _handleError = (response: ApiModel.Response): void => {
    const error = response.error as ApiModel.Error;
    switch (error.type)
    {
      case "duplicate key":
        (this.registerForm.get(error.value as string) as FormControl).setErrors({ 'duplicate': true });
        break;

      case "invalid field":
        console.error("Invalid field(s) in request");
        for (let value of error.values as string[])
        {
          (this.registerForm.get(error[value] as string) as FormControl).setErrors({ 'invalid': true });
        }
        break;

      case "unknown":
        console.error("Unknown error")
    }
  }

  /**
   * Handles response received from server after submitting register request.
   *
   * @method
   * @private
   * @returns {Promise<boolean> | void}
   */
  private _handleResponse = (response: ApiModel.Response): Promise<boolean> | void => {
    if (response.success)
    {
      return this._router.navigate(["/login"]);
    }
    else
    {
      this._handleError(response);
      return;
    }
  }

  /**
   * Tries to register the user.
   *
   * @method
   * @private
   * @returns {void}
   */
  private _registerUser = (): void => {
    const user: NewUser = {
      firstName: (this.registerForm.controls['firstName'].value as string).trim() === "" ? undefined
        : this.registerForm.controls['firstName'].value as string,
      lastName: (this.registerForm.controls['lastName'].value as string).trim() === "" ? undefined
        : this.registerForm.controls['lastName'].value as string,
      username: this.registerForm.controls['username'].value as string,
      email: (this.registerForm.controls['email'].value as string).trim() === "" ? undefined
        : this.registerForm.controls['email'].value as string,
      password: this.registerForm.controls['passwordConfirmation'].value as string,
      agreedToTermsAndConditions: this.registerForm.controls['agreedToTermsAndConditions'].value as boolean,
    }

    this._apiService.registerUser(user)
      .subscribe((response: ApiModel.Response) => {
        this._handleResponse(response);
      });
  }
}
