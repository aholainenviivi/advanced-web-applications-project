import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from "@angular/forms";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { NgIf, NgOptimizedImage } from "@angular/common";

import { ApiService } from "../../services/api.service";
import { Response as ApiResponse, Error as ApiError } from "../../models/api.model";
import { Router, RouterLink } from "@angular/router";
import { AuthService } from "../../services/auth.service";

/**
 * Component for logging in user.
 */
@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    NgIf,
    NgOptimizedImage,
    RouterLink
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit
{
  /**
   * Has log in form been submitted.
   *
   * @protected
   * @type {boolean}
   */
  protected submitted: boolean = false;

  protected loginForm = new FormGroup({
    username: new FormControl("", [
      Validators.required,
      Validators.pattern(new RegExp("^(?!\\s*$).+"))
    ]),
    password: new FormControl("", [
      Validators.required,
      Validators.pattern(new RegExp("^(?!\\s*$).+"))
    ])
  })

  constructor(private _translate: TranslateService,
              private _apiService: ApiService,
              private _router: Router,
              private _authService: AuthService) {}

  ngOnInit()
  {
    this._translate.get("login").subscribe();
  }

  /**
   * Tries to log user in if login form is valid.
   *
   * @method
   * @protected
   * @returns {void}
   */
  protected onSubmit(): void
  {
    this.submitted = true;
    if (this.loginForm.invalid) return;

    this._loginUser();
  }

  /**
   * Saves authentication token and navigates user to dashboard.
   *
   * @method
   * @private
   * @param {string} token
   * @returns {Promise<boolean>}
   */
  private _handleSuccessfulLogin = (token: string): Promise<boolean> => {
    // save auth token
    this._authService.saveToken(token);

    return this._router.navigate(["/dashboard"]);
  }

  /**
   * Handles errors resulted from log in attempt.
   *
   * @method
   * @private
   * @param {ApiError} error
   * @return {void}
   */
  private _handleError = (error: ApiError): void => {
    switch (error.type) {
      case "invalid credentials":
        this.loginForm.setErrors({ credentials: true });
        break;
      case "unknown":
        this.loginForm.setErrors({ unknown: true });
        break;
      default:
        this.loginForm.setErrors({ unknown: true });
        break;
    }
  }

  /**
   * Handles response from server.
   *
   * @method
   * @private
   * @param {ApiResponse} response
   * @returns {Promise<boolean> | void}
   */
  private _handleResponse = (response: ApiResponse): Promise<boolean> | void => {
    if (response.success) {
      return this._handleSuccessfulLogin(response.value as string);
    } else {
      return this._handleError(response.error as ApiError);
    }
  }

  /**
   * Tries to log user in.
   *
   * @method
   * @private
   * @returns {void}
   */
  private _loginUser = (): void => {
    this._apiService.loginUser(this.loginForm.value.username as string, this.loginForm.value.password as string)
      .subscribe((response: ApiResponse) => {
        this._handleResponse(response);
      });
  }
}
