import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForOf, NgIf } from "@angular/common";
import { NgbDropdown, NgbDropdownItem, NgbDropdownMenu, NgbDropdownToggle } from "@ng-bootstrap/ng-bootstrap";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { interval, of, startWith, Subscription, switchMap } from "rxjs";
import { FormControl, ReactiveFormsModule, Validators } from "@angular/forms";

import { ApiService } from "../../services/api.service";
import * as ApiModel from "../../models/api.model";
import * as ChatModel from "../../models/chat.model";
import * as UserModel from "../../models/user.model";

/**
 * Component for displaying the chatting page. The component has three possible views: the chat listing, the chat with
 * other user and the profile of the other user. From chat listing the user can open an individual chat. In the chat
 * user can go back to chat listing, send messages and open the other users profile. In the profile view user can go
 * back to chat or block the other user.
 */
@Component({
  selector: 'app-chat',
  standalone: true,
  imports: [
    NgIf,
    NgbDropdown,
    NgbDropdownItem,
    NgbDropdownMenu,
    NgbDropdownToggle,
    TranslateModule,
    NgForOf,
    ReactiveFormsModule
  ],
  templateUrl: './chat.component.html',
  styleUrl: './chat.component.css'
})
export class ChatComponent implements OnInit, OnDestroy
{
  /**
   * Subscription for polling the server for updates.
   *
   * @private
   * @type {Subscription}
   */
  private polling!: Subscription;

  // TODO split the different views to individual components
  /**
   * Indicates what part of the chatting page is shown.
   *
   * @protected
   * @type {"menu" | "chat" | "profile"}
   */
  protected currentView: "menu" | "chat" | "profile" = "menu";

  /**
   * Stores overview of chats the user is part of.
   *
   * @protected
   * @type {ChatModel.Overview[]}
   */
  protected chats: ChatModel.Overview[] = [];

  /**
   * What page of chat list pager user is viewing.
   *
   * @protected
   * @type {number}
   */
  protected chatListPage: number = 1;

  /**
   * Page numbers of chat page listing (5 chats per page).
   *
   * @protected
   * @type {Array<number>}
   */
  protected chatListPageNumbers: number[] = [];

  /**
   * Stores information of the other user in the currently open chat. Is null when viewing the chat listing.
   *
   * @protected
   * @type {UserModel.MatchUser | null}
   */
  protected currentChatPartner: UserModel.MatchUser | null = null;

  /**
   * Stores messages in the currently open chat.
   *
   * @protected
   * @type {ChatModel.Message[]}
   */
  protected currentChatMessages: ChatModel.Message[] = [];

  /**
   * FormControl for the new message view in chatting.
   *
   * @protected
   * @type {FormControl<string>}
   */
  protected newMessage: FormControl = new FormControl<string>("", [
    Validators.required,
    Validators.pattern(new RegExp("^(?!\\s*$).+"))
  ]);

  constructor(private _translate: TranslateService,
              private _apiService: ApiService) {}

  ngOnInit()
  {
    // get translations
    this._translate.get("chat");

    // get username from route and check if user is going straight to some chat
    const state = history.state;
    if (state && state.username)
    {
      console.log(state.username)
      this._getMatchProfile(state.username)
        .then(() => { this.currentView = "chat"; });
    }

    // poll server for updates on chats
    this.polling = interval(1000)
      .pipe(
        startWith(0),
        switchMap(() => {
          // poll for updates on all chats
          if (this.currentView === "menu")
          {
            return of(this._getChats());
          }
          // poll for new messages in current chat
          else if (this.currentView === "chat")
          {
            return of(this._getNewChatMessages());
          }
          return of();
        })
      ).subscribe();
  }

  ngOnDestroy()
  {
    this.polling.unsubscribe();
  }

  /**
   * Toggles the active view.
   *
   * @method
   * @protected
   * @async
   * @param {"menu" | "chat" | "profile"} view
   * @param {ChatModel.Overview | undefined} chat
   * @returns {Promise<void>}
   */
  protected async toggleView(view: "menu" | "chat" | "profile", chat?: ChatModel.Overview): Promise<void>
  {
    switch (view)
    {
      case "menu":
        this.currentChatPartner = null;
        this.newMessage.reset();
        break;

      case "chat":
        if (chat !== undefined)
        {
          await this._getMatchProfile((chat as ChatModel.Overview).chatPartner);
          // TODO get only x amount of newest messages and get more later if required
          this._getChatMessages();
        }
        break;

      case "profile":

        break;
    }
    this.currentView = view;
  }

  /**
   * Switches the page shown in chat listing.
   *
   * @method
   * @protected
   * @param {number | "first" | "previous" | "next" | "last"} goTo
   * @protected
   */
  protected switchListPage(goTo: number | "first" | "previous" | "next" | "last")
  {
    if (typeof goTo === "number")
    {
      this.chatListPage = goTo;
    }
    else
    {
      switch (goTo)
      {
        case "first":
          this.chatListPage = 1;
          break;

        case "previous":
          this.chatListPage = this.chatListPage - 1 === 0 ? 1 : this.chatListPage - 1;
          break;

        case "next":
          this.chatListPage = this.chatListPage + 1 === (this.chatListPageNumbers.at(-1) as number)+2 ? (this.chatListPageNumbers.at(-1) as number)+1 : this.chatListPage + 1;
          break;

        case "last":
          this.chatListPage = (this.chatListPageNumbers.at(-1) as number)+1;
          break;
      }
    }
  }

  /**
   * Is user viewing the first page of chat listing.
   *
   * @method
   * @protected
   * @returns {boolean}
   */
  protected isFirstPage(): boolean
  {
    return this.chatListPage === 1;
  }

  /**
   * Is user viewing the last page of chat listing.
   *
   * @method
   * @protected
   * @returns {boolean}
   */
  protected isLastPage(): boolean
  {
    return this.chatListPage === (this.chatListPageNumbers.at(-1) as number)+1;
  }

  /**
   * Sends the message of newMessage FormControl.
   *
   * @method
   * @protected
   * @returns {void}
   */
  protected sendMessage(): void
  {
    // cannot send empty message
    if (this.newMessage.status === "INVALID") return;

    this._apiService.sendChatMessage((this.currentChatPartner as UserModel.MatchUser).username, this.newMessage.value).subscribe((response) => {
      if (!response.success)
      {
        console.error("An error occurred.");
        return;
      }

      (response.value as ChatModel.Message).sentAt = new Date((response.value as ChatModel.Message).sentAt as unknown as string);

      this.currentChatMessages.push(response.value as ChatModel.Message);
      this.newMessage.reset();
    });
  }

  /**
   * Blocks the other user of currently open chat and toggles menu.
   *
   * @method
   * @protected
   * @returns {void}
   */
  protected blockMatch(): void
  {
    this._apiService.blockMatch((this.currentChatPartner as UserModel.MatchUser).username).subscribe(async (result) => {
      if (!result.success)
      {
        console.error("An error occurred.");
      }
      else
      {
        await this.toggleView("menu");
      }
    });
  }

  /**
   * Fetches chats the user is part of.
   *
   * @method
   * @private
   * @returns {Subscription}
   */
  private _getChats = (): Subscription => {
    return this._apiService.getChats().subscribe((response: ApiModel.Response): void => {
      if (!response.success)
      {
        console.error("An error occurred.");
      }
      else
      {
        // TODO fix all of this type of issues
        // response has been sent from server as json object -> date and number objects have been converted to string
        // convert the strings to correct types
        // casting to unknown then to string because typescript thinks the values are correct type
        for (let chat of response.values as ChatModel.Overview[])
        {
          chat.latestMessageSentAt = chat.latestMessageSentAt === null ? null : new Date(chat.latestMessageSentAt as unknown as string);
          chat.unreadMessagesCount = parseInt(chat.unreadMessagesCount as unknown as string);
        }
        this.chats = response.values as ChatModel.Overview[];
        this.chatListPageNumbers = [...Array(Math.ceil(this.chats.length/5)).keys()];
      }
    });
  }

  /**
   * Fetches all chat messages of currently open chat.
   *
   * @method
   * @private
   * @returns {void}
   */
  private _getChatMessages = (): void => {
    this._apiService.getChatMessages((this.currentChatPartner as UserModel.MatchUser).username).subscribe((response) => {
      if (!response.success)
      {
        console.error("An error occurred.");
      }
      else
      {
        // response has been sent from server as json object -> date object has been converted to string
        // convert the strings to correct types
        // casting to unknown then to string because typescript thinks the values are correct type
        for (let message of response.values as ChatModel.Message[])
        {
          message.sentAt = new Date(message.sentAt as unknown as string);
        }

        // assign response to current chat messages
        this.currentChatMessages = response.values as ChatModel.Message[];
      }
    });
  }

  /**
   * Fetches unread chat messages of currently open chat.
   *
   * @method
   * @private
   * @returns {Subscription}
   */
  private _getNewChatMessages = (): Subscription => {
    return this._apiService.getNewChatMessages((this.currentChatPartner as UserModel.MatchUser).username).subscribe((response) => {
      if (!response.success)
      {
        console.error("An error occurred.");
      }
      else
      {
        // response has been sent from server as json object -> date object has been converted to string
        // convert the strings to correct types
        // casting to unknown then to string because typescript thinks the values are correct type
        for (let message of response.values as ChatModel.Message[])
        {
          message.sentAt = new Date(message.sentAt as unknown as string);
        }

        // add new messages to array
        this.currentChatMessages.push(...response.values as ChatModel.Message[]);
      }
    })
  }

  /**
   * Fetches the other user's public profile of currently open chat.
   *
   * @method
   * @private
   * @param {string} username
   * @returns {Promise<void>}
   */
  private _getMatchProfile = (username: string): Promise<void> => {
    return new Promise<void>((resolve) => {
      this._apiService.getMatchProfile(username).subscribe((result) => {
        if (!result.success)
        {
          console.error("An error occurred.");
        }
        else
        {
          this.currentChatPartner = result.value as UserModel.MatchUser;
        }
        resolve();
      });
    });
  }
}
