import { Component, OnInit } from '@angular/core';
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { NgIf } from "@angular/common";
import { NavigationExtras, Router } from "@angular/router";

import * as UserModel from "../../models/user.model";
import { ApiService } from "../../services/api.service";

/**
 * Component for displaying matchmaking of the application. The matches are show with cards and user can like or reject
 * the other user. If match is found, an interface prompting user to start chat or go back to matching is shown. If there
 * is no users to show, an interface alerting of that is shown.
 */
@Component({
  selector: 'app-matchmaker',
  standalone: true,
  imports: [
    TranslateModule,
    NgIf
  ],
  templateUrl: './matchmaker.component.html',
  styleUrl: './matchmaker.component.css'
})
export class MatchmakerComponent implements OnInit
{
  /**
   * Profiles of other users.
   *
   * @protected
   * @type {UserModel.MatchUser[]}
   */
  protected matchProfiles: UserModel.MatchUser[] = [];

  /**
   * Profile of the current user shown.
   *
   * @protected
   * @type {UserModel.MatchUser}
   */
  protected currentProfile!: UserModel.MatchUser;

  /**
   * Did the last like result in match.
   *
   * @protected
   * @type {boolean}
   */
  protected matchFound: boolean = false;

  /**
   * Server has no more profiles to send.
   *
   * @type {boolean}
   */
  noProfilesToShow: boolean = false;

  constructor(private _translate: TranslateService,
              private _apiService: ApiService,
              private _router: Router) {}

  ngOnInit()
  {
    this._translate.get("matchmaker");
    this._getMatchProfiles()
      .then(() => this._showNextProfile() );
  }

  /**
   * Rejects the currently displayed user.
   *
   * @method
   * @async
   * @protected
   * @returns {Promise<void>}
   */
  protected async onReject(): Promise<void>
  {
    this._apiService.sendRejection(this.currentProfile.username).subscribe((response) => {
      if (!response.success)
      {
        console.error("An error occurred.");
      }
    });
    await this._showNextProfile();
  }

  /**
   * Likes the currently shown user.
   *
   * @method
   * @async
   * @protected
   * @returns {Promise<void>}
   */
  protected async onLike(): Promise<void>
  {
    await new Promise<void>((resolve, reject) => {
      this._apiService.sendLike(this.currentProfile.username).subscribe(async (response) => {
        if (!response.success) {
          console.error("An error occurred.");
        }
        // match found
        if (response.value) {
          this.matchFound = true;
          // add new chat to database
          this._apiService.startChat(this.currentProfile.username).subscribe((result) => {
            if (!result.success) {
              console.error("Error occurred.");
            }
          });
        } else {
          await this._showNextProfile()
        }
        resolve();
      })
    });
  }

  /**
   * Starts chat with the newly found match.
   *
   * @method
   * @protected
   * @async
   * @returns {Promise<void>}
   */
  protected async startChat(): Promise<void>
  {
    const navigationExtras: NavigationExtras = {
      state: {
        username: this.currentProfile.username
      }
    }
    await this._router.navigate(["/chats"], navigationExtras);
  }

  /**
   * Navigates user back to matching.
   *
   * @method
   * @protected
   * @async
   * @returns {Promise<void>}
   */
  protected async goBack(): Promise<void>
  {
    this.matchFound = false;
    await this._showNextProfile();
  }

  /**
   * Fetches profiles of other users to show for matching.
   *
   * @method
   * @private
   * @returns {Promise<void>}
   */
  private _getMatchProfiles = (): Promise<void> => {
    return new Promise<void>((resolve) => {
      this._apiService.getMatchProfiles().subscribe((response) => {
        if (response.success) {
          this.matchProfiles.push(...response.values as UserModel.MatchUser[]);
        }
        if (this.matchProfiles.length == 0)
        {
          this.noProfilesToShow = true;
        }
        resolve();
      });
    });
  }

  /**
   * Replaces the current user shown with new user.
   *
   * @method
   * @private
   * @async
   * @returns {Promise<void>}
   */
  private _showNextProfile = async (): Promise<void> => {
    // TODO fetch new profiles before the client runs out of profiles to show
    if (this.matchProfiles.length === 0)
    {
      await this._getMatchProfiles();
    }
    this.currentProfile = this.matchProfiles.shift() as UserModel.MatchUser;
  }
}
