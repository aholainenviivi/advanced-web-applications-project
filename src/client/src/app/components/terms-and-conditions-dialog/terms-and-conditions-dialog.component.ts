import { Component, OnInit } from '@angular/core';
import { MatButton } from "@angular/material/button";
import { MatDialogActions, MatDialogClose, MatDialogContent, MatDialogTitle } from "@angular/material/dialog";
import { TranslateModule, TranslateService } from "@ngx-translate/core";

/**
 * Component that displays modal with terms and conditions.
 */
@Component({
  selector: 'app-terms-and-conditions-dialog',
  standalone: true,
  imports: [
    MatButton,
    MatDialogClose,
    MatDialogActions,
    MatDialogTitle,
    MatDialogContent,
    TranslateModule
  ],
  templateUrl: './terms-and-conditions-dialog.component.html',
  styleUrl: './terms-and-conditions-dialog.component.css'
})
export class TermsAndConditionsDialogComponent implements OnInit
{
  constructor(private _translate: TranslateService) {}

  ngOnInit()
  {
    this._translate.get('terms_and_conditions').subscribe();
  }
}
