import { Component, OnInit } from '@angular/core';
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { Router, RouterLink } from "@angular/router";
import { NgOptimizedImage } from "@angular/common";

/**
 * Component that displays the home page of the application. Has marketing text, call to action and button to redirect
 * user to register and login.
 */
@Component({
  selector: 'app-home',
  standalone: true,
  imports: [TranslateModule, RouterLink, NgOptimizedImage],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit
{
  constructor(private _translate: TranslateService) {}

  ngOnInit()
  {
    this._translate.get("home").subscribe();
  }
}
