import { Injectable } from '@angular/core';
import { Router } from "@angular/router";

/**
 * Service to handle user authentication.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService
{
  constructor(private router: Router) {}

  /**
   * Authentication token
   *
   * @type {string | null}
   */
  public get token()
  {
    return localStorage.getItem("auth_token");
  }

  /**
   * Saves authentication token to local storage.
   *
   * @param {string} token
   * @returns {void}
   */
  public saveToken(token: string): void
  {
    localStorage.setItem("auth_token", token);
  }

  /**
   * Deletes authentication token from local storage.
   *
   * @returns {void}
   */
  public deleteToken(): void
  {
    localStorage.removeItem("auth_token");
  }

  /**
   * Checks if user is logged in.
   *
   * @returns {boolean}
   */
  public isLoggedIn = (): boolean => {
    const token = this.token;

    // check if token exists
    if (token === undefined || token === null) return false;

    // check if token has expired
    const jwtPayload = JSON.parse(window.atob((token as string).split('.')[1]));
    if (Date.now() > jwtPayload.exp * 1000) return false;   // * 1000 because Date.now() precision is milliseconds and JWT token exp is seconds

    return true;
  }

  /**
   * Logs user out.
   *
   * @returns {Promise<boolean>}
   */
  public logout = (): Promise<boolean> => {
    localStorage.clear();
    return this.router.navigate(["/login"]);
  }
}
