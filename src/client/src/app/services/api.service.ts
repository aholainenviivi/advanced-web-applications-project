import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { catchError, Observable, Subscriber, throwError } from "rxjs";

import * as UserModel from "../models/user.model";
import * as ApiModel from "../models/api.model";

import { AuthService } from "./auth.service";

/**
 * Service to communicate with the API.
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService
{
  constructor(private _http: HttpClient,
              private _authService: AuthService) { }

  /**
   * Sends register request to API.
   *
   * @param {UserModel.NewUser} user
   * @returns {Observable<ApiModel.Response>}
   */
  public registerUser = (user: UserModel.NewUser): Observable<ApiModel.Response> => {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json"
      }
    };

    return this._http.post<ApiModel.Response>("/api/users/register", user, httpOptions)
      .pipe(
        catchError(this._handleError)
      );
  }

  /**
   * Sends login request to API.
   *
   * @param {string} username
   * @param {string} password
   * @returns {Observable<ApiModel.Response>}
   */
  public loginUser = (username: string, password: string): Observable<ApiModel.Response> => {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json"
      }
    };

    return this._http.post<ApiModel.Response>("/api/users/login", { username, password }, httpOptions)
      .pipe(
        catchError(this._handleError)
      );
  }

  /**
   * Fetches user profile from API.
   *
   * @returns {Observable<ApiModel.Response>}
   */
  public getUserProfile = (): Observable<ApiModel.Response> => {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `bearer ${this._authService.token}`
      }
    };

    return this._http.get<ApiModel.Response>("/api/users/profile", httpOptions)
      .pipe(
        catchError(this._handleError)
      );
  }

  /**
   * Sends profile update request to API.
   *
   * @param {UserModel.User} user
   * @returns {Observable<ApiModel.Response>}
   */
  public updateUserProfile = (user: UserModel.User): Observable<ApiModel.Response> => {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `bearer ${this._authService.token}`
      }
    };

    return this._http.post<ApiModel.Response>("/api/users/profile", user, httpOptions)
      .pipe(
        catchError(this._handleError)
      );
  }

  /**
   * Fetches other users' profiles from the API.
   *
   * @returns {Observable<ApiModel.Response>}
   */
  public getMatchProfiles(): Observable<ApiModel.Response>
  {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `bearer ${this._authService.token}`
      }
    };

    return this._http.get<ApiModel.Response>("/api/users/matches", httpOptions)
      .pipe(
        catchError(this._handleError)
      );
  }

  /**
   * Fetches profile of chat partner from API.
   *
   * @param {string} chatPartnerUsername
   * @returns {Observable<ApiModel.Response>}
   */
  public getMatchProfile(chatPartnerUsername: string): Observable<ApiModel.Response>
  {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `bearer ${this._authService.token}`
      }
    };

    return this._http.get<ApiModel.Response>(`/api/users/matches/${chatPartnerUsername}`, httpOptions)
      .pipe(
        catchError(this._handleError)
      )
  }

  public deleteUser(): Observable<ApiModel.Response>
  {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `bearer ${this._authService.token}`
      }
    };

    return this._http.delete<ApiModel.Response>(`/api/users`, httpOptions)
      .pipe(
        catchError(this._handleError)
      )
  }

  /**
   * Sends rejection to API.
   *
   * @param {string} rejectedUsername
   * @returns {Observable<ApiModel.Response>}
   */
  public sendRejection = (rejectedUsername: string): Observable<ApiModel.Response> => {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `bearer ${this._authService.token}`
      }
    };

    return this._http.post<ApiModel.Response>("/api/matchmaker/reject", { username: rejectedUsername }, httpOptions)
      .pipe(
        catchError(this._handleError)
      );
  }

  /**
   * Sends like to API.
   *
   * @param {string} likedUsername
   * @returns {Observable<ApiModel.Response>}
   */
  public sendLike(likedUsername: string): Observable<ApiModel.Response> {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `bearer ${this._authService.token}`
      }
    };

    return this._http.post<ApiModel.Response>("/api/matchmaker/like", { username: likedUsername }, httpOptions)
      .pipe(
        catchError(this._handleError)
      );
  }

  /**
   * Sends block to API.
   *
   * @param {string} blockedUsername
   * @returns {Observable<ApiModel.Response>}
   */
  public blockMatch(blockedUsername: string): Observable<ApiModel.Response>
  {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `bearer ${this._authService.token}`
      }
    };

    return this._http.post<ApiModel.Response>(`/api/matchmaker/block`, { username: blockedUsername }, httpOptions)
      .pipe(
        catchError(this._handleError)
      )
  }

  /**
   * Sends chat start request to API.
   *
   * @param {string} chatPartnerUsername
   * @returns {Observable<ApiModel.Response>}
   */
  public startChat = (chatPartnerUsername: string): Observable<ApiModel.Response> => {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `bearer ${this._authService.token}`
      }
    };

    return this._http.post<ApiModel.Response>("/api/chats", { username: chatPartnerUsername }, httpOptions)
      .pipe(
        catchError(this._handleError)
      )
  }

  /**
   * Fetches chats the user is part of from API.
   *
   * @returns {Observable<ApiModel.Response>}
   */
  public getChats(): Observable<ApiModel.Response>
  {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `bearer ${this._authService.token}`
      }
    };

    return this._http.get<ApiModel.Response>("/api/chats", httpOptions)
      .pipe(
        catchError(this._handleError)
      )
  }

  /**
   * Fetches chat messages with specified user from API.
   *
   * @param {string} chatPartnerUsername
   * @returns {Observable<ApiModel.Response>}
   */
  public getChatMessages(chatPartnerUsername: string): Observable<ApiModel.Response>
  {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `bearer ${this._authService.token}`
      }
    };

    return this._http.get<ApiModel.Response>(`/api/chats/messages/${chatPartnerUsername}`, httpOptions)
      .pipe(
        catchError(this._handleError)
      )
  }

  /**
   * Fetches unread messages received from specified user from API.
   *
   * @param {string} chatPartnerUsername
   * @returns {Observable<ApiModel.Response>}
   */
  public getNewChatMessages(chatPartnerUsername: string): Observable<ApiModel.Response>
  {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `bearer ${this._authService.token}`
      }
    };

    return this._http.get<ApiModel.Response>(`/api/chats/messages/${chatPartnerUsername}/new`, httpOptions)
      .pipe(
        catchError(this._handleError)
      )
  }

  /**
   * Sends message send request to API.
   *
   * @param {string} receiverUsername
   * @param {string} content
   * @returns {Observable<ApiModel.Response>}
   */
  public sendChatMessage(receiverUsername: string, content: string): Observable<ApiModel.Response>
  {
    const httpOptions = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `bearer ${this._authService.token}`
      }
    };

    return this._http.post<ApiModel.Response>(`/api/chats/messages`, { username: receiverUsername, content }, httpOptions)
      .pipe(
        catchError(this._handleError)
      )
  }

  /**
   * Handles error responses received from API.
   *
   * @param {HttpErrorResponse} error
   * @returns {Observable<ApiModel.Response>}
   */
  private _handleError = (error: HttpErrorResponse): Observable<ApiModel.Response> => {
    return new Observable<ApiModel.Response>((subscriber) => {
      // Unauthorized and not related to invalid credentials
      if (error.status === 401 && error.error === undefined)
      {
        this._authService.logout().then();
        return;
      }
      subscriber.next(error.error)
    });
  }
}
