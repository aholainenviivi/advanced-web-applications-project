import { Routes } from '@angular/router';
import { authGuard } from "./guards/auth.guard";
import { RegisterComponent } from "./components/register/register.component";
import { LoginComponent } from "./components/login/login.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { MatchmakerComponent } from "./components/matchmaker/matchmaker.component";
import { ChatComponent } from "./components/chat/chat.component";
import { HomeComponent } from "./components/home/home.component";

// TODO add translation to route titles
export const routes: Routes = [
  { path: "", component: HomeComponent, title: "Bootleg Tinder" },
  { path: "register", component: RegisterComponent, title: "Bootleg Tinder - Register" },
  { path: "login", component: LoginComponent, title: "Bootleg Tinder - Log In" },
  { path: "dashboard", component: DashboardComponent, title: "Bootleg Tinder", canActivate: [authGuard] },
  { path: "profile", component: ProfileComponent, title: "Bootleg Tinder - Profile", canActivate: [authGuard] },
  { path: "matchmaker", component: MatchmakerComponent, title: "Bootleg Tinder - Matchmaker", canActivate: [authGuard] },
  { path: "chats", component: ChatComponent, title: "Bootleg Tinder - Chats", canActivate: [authGuard] },
];
