/**
 * Represents information required/available to register new user.
 *
 * @interface
 * @index string
 * @value {string | boolean | undefined}
 */
export interface NewUser
{
  firstName?: string,
  lastName?: string,
  username: string,
  email?: string,
  password: string,
  agreedToTermsAndConditions: boolean
}

/**
 * Represents information of user fetchable from server.
 *
 * @interface
 * @index string
 * @value {string | boolean | Date | null | undefined}
 */
export interface User
{
  [index: string]: string | boolean | Date | null | undefined

  firstName: string | null,
  lastName: string | null,
  username: string,
  email: string | null,
  bio: string | null,
  registeredAt?: Date,
  isAdmin?: boolean
}

/**
 * Represents information of other users fetchable from server.
 *
 * @interface
 * @index string
 * @value {string | boolean | Date | null | undefined}
 */
export interface MatchUser
{
  [index: string]: any

  firstName: string | null,
  lastName: string | null,
  username: string,
  bio: string | null
}
