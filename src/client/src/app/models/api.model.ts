import * as UserModel from "./user.model";
import * as ChatModel from "./chat.model";

/**
 * Represents response received from the API.
 *
 * @interface
 * @index string
 * @value {boolean | string | Error | UserModel.User | UserModel.MatchUser[] | ChatModel.Message
 *     | ChatModel.Message[] | ChatModel.Overview[] | undefined}
 */
export interface Response
{
  [index: string]: boolean | string | Error | UserModel.User | UserModel.MatchUser[] | ChatModel.Message
    | ChatModel.Message[] | ChatModel.Overview[] | undefined

  success: boolean,
  msg?: string,
  error?: Error,
  value?: UserModel.User | boolean | ChatModel.Message | string,
  values?: UserModel.MatchUser[] | ChatModel.Overview[] | ChatModel.Message[]
}

/**
 * Represents error that is included in API response.
 *
 * @interface
 * @index string
 * @value {string | string[] | undefined}
 */
export interface Error
{
  [index: string]: string | string[] | undefined

  type: "invalid field" | "duplicate key" | "invalid credentials" | "unknown",
  values?: string[]
  value?: string
}
