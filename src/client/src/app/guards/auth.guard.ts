import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot } from '@angular/router';
import { inject } from "@angular/core";

import { AuthService } from "../services/auth.service";

/**
 * Checks if user is logged in. Returns true if true, clears the local storage and navigates user to login page if false.
 *
 * @param {ActivatedRouteSnapshot} route
 * @param {RouterStateSnapshot} state
 */
export const authGuard: CanActivateFn = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> => {
  const authService = inject(AuthService);
  const router = inject(Router);

  // User is logged in
  if (authService.isLoggedIn())
  {
    return true;
  }

  // User isn't logged in
  localStorage.clear();
  return router.navigate(["/login"]);
};
