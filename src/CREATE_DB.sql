-- TABLES
CREATE TABLE users (
    user_id UUID UNIQUE DEFAULT gen_random_UUID(),
    first_name VARCHAR,
    last_name VARCHAR,
    username VARCHAR UNIQUE NOT NULL,
    bio VARCHAR(1000),
    email VARCHAR UNIQUE,
    password VARCHAR NOT NULL,
    registered_at TIMESTAMP DEFAULT current_timestamp,
    last_agreed_to_terms_and_conditions_at TIMESTAMP DEFAULT current_timestamp,
    is_admin BOOLEAN DEFAULT FALSE,

    PRIMARY KEY (user_id)
);

CREATE TABLE likes (
    liker_user_id UUID NOT NULL,
    liked_user_id UUID NOT NULL,
    CONSTRAINT unique_like UNIQUE (liker_user_id, liked_user_id),

    PRIMARY KEY (liker_user_id, liked_user_id),
    FOREIGN KEY (liker_user_id) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (liked_user_id) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE matches (
    match_id UUID UNIQUE DEFAULT gen_random_UUID(),
    user1 UUID NOT NULL,
    user2 UUID NOT NULL,
    matched_at TIMESTAMP DEFAULT current_timestamp,

    PRIMARY KEY (match_id),
    FOREIGN KEY (user1) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (user2) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (user1, user2) REFERENCES likes(liker_user_id, liked_user_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (user1, user2) REFERENCES likes(liked_user_id, liker_user_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (user2) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE UNIQUE INDEX matches_users_unique_combination ON matches (LEAST(user1, user2), GREATEST(user1, user2));

CREATE TABLE rejections (
    rejecter_user_id UUID NOT NULL,
    rejected_user_id UUID NOT NULL,
    CONSTRAINT unique_rejection UNIQUE (rejecter_user_id, rejected_user_id),

    PRIMARY KEY (rejecter_user_id, rejected_user_id),
    FOREIGN KEY (rejecter_user_id) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (rejected_user_id) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE blocks (
    blocker_user_id UUID NOT NULL,
    blocked_user_id UUID NOT NULL,
    CONSTRAINT unique_block UNIQUE (blocker_user_id, blocked_user_id),

    PRIMARY KEY (blocker_user_id, blocked_user_id),
    FOREIGN KEY (blocker_user_id) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (blocked_user_id) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE chats (
    chat_id UUID UNIQUE DEFAULT gen_random_UUID(),
    user1_id UUID NOT NULL,
    user2_id UUID NOT NULL,
    match_id UUID NOT NULL,
    started_at TIMESTAMP DEFAULT current_timestamp,

    PRIMARY KEY (chat_id),
    FOREIGN KEY (user1_id) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (user2_id) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (match_id) REFERENCES matches(match_id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE UNIQUE INDEX chats_users_unique_combination ON chats (LEAST(user1_id, user2_id), GREATEST(user1_id, user2_id));

CREATE TABLE chat_messages (
    msg_id UUID UNIQUE DEFAULT gen_random_UUID(),
    user_id UUID NOT NULL,
    chat_id UUID NOT NULL,
    sent_at TIMESTAMP DEFAULT current_timestamp,
    is_read BOOLEAN DEFAULT FALSE,
    content VARCHAR NOT NULL,

    PRIMARY KEY (msg_id),
    FOREIGN KEY (chat_id) REFERENCES chats(chat_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE
);

-- Idea and help for preventing column alters from following StackOverflow/StackExchange answers
-- https://dba.stackexchange.com/a/194850
-- https://stackoverflow.com/a/57735867
CREATE FUNCTION prevent_alter()
    RETURNS TRIGGER
    LANGUAGE plpgsql
AS $$
BEGIN
    RAISE EXCEPTION 'Altering column not allowed';
END;
$$;

CREATE FUNCTION remove_connections()
    RETURNS TRIGGER
    LANGUAGE plpgsql
AS $$
BEGIN
    DELETE FROM likes
    WHERE (liker_user_id = NEW.blocker_user_id AND liked_user_id = NEW.blocked_user_id)
       OR (liker_user_id = NEW.blocked_user_id AND liked_user_id = NEW.blocker_user_id);
    RETURN NULL;
END;
$$;

-- TRIGGERS
CREATE TRIGGER prevent_alter_user_registered_at
    BEFORE UPDATE OF registered_at ON users
    FOR EACH ROW
    WHEN (NEW.registered_at IS DISTINCT FROM OLD.registered_at)
    EXECUTE PROCEDURE prevent_alter();

CREATE TRIGGER prevent_alter_chat_started_at
    BEFORE UPDATE OF started_at ON chats
    FOR EACH ROW
    WHEN (NEW.started_at IS DISTINCT FROM OLD.started_at)
    EXECUTE PROCEDURE prevent_alter();

CREATE TRIGGER prevent_alter_chat_message_sent_at
    BEFORE UPDATE OF sent_at ON chat_messages
    FOR EACH ROW
    WHEN (NEW.sent_at IS DISTINCT FROM OLD.sent_at)
    EXECUTE PROCEDURE prevent_alter();

CREATE TRIGGER prevent_alter_matches_matched_at
    BEFORE UPDATE OF matched_at ON matches
    FOR EACH ROW
    WHEN (NEW.matched_at IS DISTINCT FROM OLD.matched_at)
    EXECUTE PROCEDURE prevent_alter();

CREATE TRIGGER remove_connections_on_block
    AFTER INSERT ON blocks
    FOR EACH ROW
    EXECUTE PROCEDURE remove_connections();

-- ADD ADMIN USER TO USERS
INSERT INTO users(first_name, last_name, username, email, password, is_admin)
    VALUES (null, null, 'admin', null, '$2a$10$G.ooIAP5QcL0C6SvgqdEXeO7lPPve23NituUB35K8GjiIm2jsFgRK ', true);
