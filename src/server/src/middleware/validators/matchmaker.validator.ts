import { Request, Response, NextFunction } from "express";
import { ValidationChain, body } from "express-validator";

import { validate } from "./validator";

/**
 * Middleware for validating user rejection requests.
 *
 * @returns {(ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[]}
 */
export const reject: (ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[] = [
    body("username").trim().notEmpty(),
    validate
];

/**
 * Middleware for validating user like requests.
 *
 * @returns {(ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[]}
 */
export const like: (ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[] = [
    body("username").trim().notEmpty(),
    validate
];

/**
 * Middleware for validating user block requests.
 *
 * @returns {(ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[]}
 */
export const block: (ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[] = [
    body("username").trim().notEmpty(),
    validate
];
