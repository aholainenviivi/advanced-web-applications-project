import { Request, Response, NextFunction } from "express";
import { body, param, ValidationChain } from "express-validator";

import { validate } from "./validator";

/**
 * Middleware for validating chat start requests.
 *
 * @returns {(ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[]}
 */
export const startChat: (ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[] = [
    body("username").trim().notEmpty(),
    validate
];

/**
 * Middleware validator for requests getting messages.
 *
 * @returns {(ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[]}
 */
export const getMessages: (ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[] = [
    param("username").trim().notEmpty(),
    validate
];

/**
 * Middleware validator for message send requests.
 *
 * @returns {(ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[]}
 */
export const sendMessage: (ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[] = [
    body(["username", "content"]).trim().notEmpty(),
    validate
];
