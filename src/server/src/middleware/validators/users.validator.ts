import { Request, Response, NextFunction } from "express";
import { ValidationChain, body, param } from "express-validator";

import { validate } from "./validator";

/**
 * Middleware for validating user registration requests.
 *
 * @returns {(ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[]}
 */
export const register: (ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[] = [
    body(["firstName", "lastName"]).optional().trim().isString().notEmpty(),
    body("username").trim().notEmpty().isString(),
    body("email").optional().trim().isEmail(),
    body("password").trim().matches(RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#$%^&*()_+{}:<>?,./;'[\\]\\\\~-])(.{8,})$")),
    body("agreedToTermsAndConditions").custom(async (value) => {
        if (!value)
        {
            throw new Error("Invalid value");
        }
    }),
    validate
];

/**
 * Middleware for validating user log in requests.
 *
 * @returns {(ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[]}
 */
export const login: (ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[] = [
    body(["username", "password"]).trim().isString().notEmpty(),
    validate
];

/**
 * Middleware for validating user profile update requests.
 *
 * @returns {(ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[]}
 */
export const updateProfile: (ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[] = [
    body(["firstName", "lastName"]).optional().trim().isString().notEmpty(),
    body("username").trim().notEmpty().isString(),
    body("bio").optional().trim().isString().notEmpty(),
    body("email").optional().trim().isEmail(),
    body("password").optional().trim().matches(RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#$%^&*()_+{}:<>?,./;'[\\]\\\\~-])(.{8,})$")),
    validate
];

/**
 * Middleware for validating requests for getting other user's profile.
 *
 * @returns {(ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[]}
 */
export const getMatchProfile: (ValidationChain | ((req: Request, res: Response, next: NextFunction) => void))[] = [
    param("username").trim().notEmpty(),
    validate
];
