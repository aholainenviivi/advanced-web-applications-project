import { NextFunction, Request, Response } from "express";
import { Result, validationResult } from "express-validator";
import { InvalidRequestFieldError } from "../../errors/errors";

/**
 * Middleware function that handles the validation result.
 *
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {void}
 */
export const validate = (req: Request, res: Response, next: NextFunction): void => {
    const errors: Result = validationResult(req);

    if (!errors.isEmpty())
    {
        const fields: string[] = [];
        for (let error of errors.array())
        {
            fields.push(error.path);
        }

        next(new InvalidRequestFieldError("Invalid field(s) in request.", fields));
    }
    next();
}
