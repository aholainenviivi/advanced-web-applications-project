import { Request, Response, NextFunction } from "express";

import { CustomError } from "../errors/errors";
import * as ApiModel from "../models/api.model";

/**
 * Custom error handler middleware.
 *
 * @param {Error | CustomError} err
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {void}
 */
export const errorHandler = (err: Error | CustomError, req: Request, res: Response, next: NextFunction): void => {
    console.error(err);

    // Custom error
    if (err instanceof CustomError)
    {
        res.status(err.statusCode).json(err.response);
    }
    // Built-in error
    else
    {
        const response: ApiModel.Response = {
            success: false,
            msg: "Internal server error.",
            error: { type: "unknown" }
        };
        res.status(500).json(response);
    }
}
