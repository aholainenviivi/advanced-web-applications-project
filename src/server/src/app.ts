import express from "express";
import path from "path";
import dotenv from "dotenv";

import DB from "./db/db";
import { configure as configurePassport } from "./auth/passport";
import router from "./routes/index";
import { errorHandler } from "./middleware/error-handler";

const app = express();

// Environment variables
dotenv.config();

// Miscellaneous middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// Passport
configurePassport().then();

// Database
const db = DB.Instance;
db.connect();

// Routes and controllers
app.use("/api", router);

// Error handler
app.use(errorHandler);

module.exports = app;
