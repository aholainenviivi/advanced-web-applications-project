import express from "express";
const router = express.Router();
import asyncHandler from "express-async-handler";
import passport from "passport";

import * as usersController from "../controllers/users.controller";
import * as usersValidator from "../middleware/validators/users.validator";

/**
 * POST /api/users/register
 * Express route for user registration.
 *
 * @function
 * @param {string} route
 * @param {Function} validator
 * @param {Function} controller
 */
router.post("/register",
    usersValidator.register,
    asyncHandler(usersController.register));

/**
 * POST /api/users/login
 * Express route for user log in.
 *
 * @function
 * @param {string} route
 * @param {Function} validator
 * @param {Function} controller
 */
router.post("/login",
    usersValidator.login,
    asyncHandler(usersController.login));

/**
 * GET /api/users/profile
 * Express route for fetching user profile.
 *
 * @function
 * @param {string} route
 * @param {Function} authenticator
 * @param {Function} controller
 */
router.get("/profile",
    passport.authenticate("jwt", { session: false }),
    asyncHandler(usersController.getUserProfile));

/**
 * POST /api/users/profile
 * Express route for updating user profile.
 *
 * @function
 * @param {string} route
 * @param {Function} authenticator
 * @param {Function} validator
 * @param {Function} controller
 */
router.post("/profile",
    passport.authenticate("jwt", { session: false }),
    usersValidator.updateProfile,
    asyncHandler(usersController.updateUserProfile));

/**
 * GET /api/users/matches
 * Express route for getting profiles of other users to show in matchmaking.
 *
 * @function
 * @param {string} route
 * @param {Function} authenticator
 * @param {Function} controller
 */
router.get("/matches/",
    passport.authenticate("jwt", { session: false }),
    asyncHandler(usersController.getMatchProfiles));

/**
 * GET /api/users/matches/:username
 * Express route for getting the profile of a user matched with.
 *
 * @function
 * @param {string} route
 * @param {Function} authenticator
 * @param {Function} validator
 * @param {Function} controller
 */
router.get("/matches/:username",
    passport.authenticate("jwt", { session: false }),
    usersValidator.getMatchProfile,
    asyncHandler(usersController.getMatchProfile));

/**
 * DELETE /api/users/
 * Express route for deleting user.
 *
 * @function
 * @param {string} route
 * @param {Function} authenticator
 * @param {Function} controller
 */
router.delete("/",
    passport.authenticate("jwt", { session: false }),
    asyncHandler(usersController.deleteUser))

export default router;
