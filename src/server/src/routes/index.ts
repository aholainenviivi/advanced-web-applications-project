import express from "express";
const router = express.Router();

import usersRouter from "./users";
import matchmakerRouter from "./matchmaker"
import chatsRouter from "./chats";

/**
 * ALL /api/users
 * Express routes for /api/users requests.
 *
 * @function
 * @param {string} route
 * @param {express.Router} router
 * */
router.use("/users", usersRouter);

/**
 * ALL /api/matchmaker
 * Express routes for /api/matchmaker requests.
 *
 * @function
 * @param {string} route
 * @param {express.Router} router
 * */
router.use("/matchmaker", matchmakerRouter);

/**
 * ALL /api/chats
 * Express routes for /api/chats requests.
 *
 * @function
 * @param {string} route
 * @param {express.Router} router
 * */
router.use("/chats", chatsRouter);

export default router;
