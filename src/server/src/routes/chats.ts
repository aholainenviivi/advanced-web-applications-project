import express from "express";
const router = express.Router();
import passport from "passport";
import asyncHandler from "express-async-handler";

import * as chatsValidator from "../middleware/validators/chats.validator";
import * as chatsController from "../controllers/chats.controller";

/**
 * POST /api/chats
 * Express route for starting new chat.
 *
 * @function
 * @param {string} route
 * @param {Function} authenticator
 * @param {Function} validator
 * @param {Function} controller
 * */
router.post("/",
    passport.authenticate("jwt", { session: false }),
    chatsValidator.startChat,
    asyncHandler(chatsController.startChat));

/**
 * GET /api/chats
 * Express route for getting chats that the user is part of.
 *
 * @function
 * @param {string} route
 * @param {Function} authenticator
 * @param {Function} controller
 * */
router.get("/",
    passport.authenticate("jwt", { session: false }),
    asyncHandler(chatsController.getChats));

/**
 * GET /api/chats/messages/:username
 * Express route for getting messages of a chat with specified user.
 *
 * @function
 * @param {string} route
 * @param {Function} authenticator
 * @param {Function} validator
 * @param {Function} controller
 */
router.get("/messages/:username",
    passport.authenticate("jwt", { session: false }),
    chatsValidator.getMessages,
    asyncHandler(chatsController.getMessages))

/**
 * GET /api/chats/messages/:username/new
 * Express route for getting unread messages of a chat with specified user.
 *
 * @function
 * @param {string} route
 * @param {Function} authenticator
 * @param {Function} validator
 * @param {Function} controller
 */
router.get("/messages/:username/new",
    passport.authenticate("jwt", { session: false }),
    chatsValidator.getMessages,
    asyncHandler(chatsController.getNewMessages));

/**
 * POST /api/chats/messages/
 * Express route for sending a message.
 *
 * @function
 * @param {string} route
 * @param {Function} authenticator
 * @param {Function} validator
 * @param {Function} controller
 */
router.post("/messages",
    passport.authenticate("jwt", { session: false }),
    chatsValidator.sendMessage,
    asyncHandler(chatsController.sendMessage))

export default router;
