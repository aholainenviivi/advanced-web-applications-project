import express from "express";
const router = express.Router();
import passport from "passport";

import * as matchmakerValidator from "../middleware/validators/matchmaker.validator";
import * as matchmakerController from "../controllers/matchmaker.controller";
import asyncHandler from "express-async-handler";

/**
 * POST /api/matchmaker/reject
 * Express route for rejecting user.
 *
 * @function
 * @param {string} route
 * @param {Function} authenticator
 * @param {Function} validator
 * @param {Function} controller
 */
router.post("/reject",
    passport.authenticate("jwt", { session: false }),
    matchmakerValidator.reject,
    asyncHandler(matchmakerController.reject));

/**
 * POST /api/matchmaker/like
 * Express route for liking user.
 *
 * @function
 * @param {string} route
 * @param {Function} authenticator
 * @param {Function} validator
 * @param {Function} controller
 */
router.post("/like",
    passport.authenticate("jwt", { session: false }),
    matchmakerValidator.like,
    asyncHandler(matchmakerController.like));

/**
 * POST /api/matchmaker/block
 * Express route for blocking user.
 *
 * @function
 * @param {string} route
 * @param {Function} authenticator
 * @param {Function} validator
 * @param {Function} controller
 */
router.post("/block",
    passport.authenticate("jwt", { session: false }),
    matchmakerValidator.block,
    asyncHandler(matchmakerController.block));

export default router;
