import { UUID } from "node:crypto";

/**
 * Represents information required to register new user.
 *
 * @interface
 * @index string
 * @type {string | null}
 */
export interface NewUser
{
    [index: string]: string | null

    first_name: string | null,
    last_name: string | null,
    username: string,
    email: string | null,
    password: string
}

/**
 * Represents user in database.
 * The optional fields are columns in the users database table that aren't always defined when handling user object outside the database.
 *
 * @interface
 * @index string
 * @type {string | boolean | UUID | Date | null | undefined}
 */
export interface User
{
    [index: string]: string | boolean | UUID | Date | null | undefined

    userId?: UUID,
    firstName: string | null,
    lastName: string | null,
    username: string,
    bio: string | null,
    email: string | null,
    password?: string,
    registeredAt: Date,
    lastAgreedToTermsAndConditions?: Date,
    isAdmin?: boolean
}

/**
 * Represent data of user that can be displayed to other users.
 *
 * @index string
 * @type {string | null}
 */
export interface MatchUser
{
    [index: string]: string | null

    firstName: string | null,
    lastName: string | null,
    username: string,
    bio: string | null
}
