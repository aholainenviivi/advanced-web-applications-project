import * as UserModel from "./user.model";
import * as ChatModel from "./chat.model";

/**
 * Represent response sent from server to client.
 *
 * @interface
 * @index string
 * @type {boolean | string | Error | UserModel.User | ChatModel.Message | UserModel.MatchUser | UserModel.MatchUser[]
 *         | ChatModel.Overview[] | ChatModel.Message[] | undefined}
 */
export interface Response
{
    [index: string]: boolean | string | Error | UserModel.User | ChatModel.Message | UserModel.MatchUser | UserModel.MatchUser[]
        | ChatModel.Overview[] | ChatModel.Message[] | undefined

    /**
     * Success of request.
     *
     * @type {boolean}
     */
    success: boolean,

    /**
     * Message that describes result of request handling.
     *
     * @type {string | undefined}
     */
    msg?: string

    /**
     * Error encountered while handling the request.
     *
     * @type {Error | undefined}
     */
    error?: Error,

    /**
     * Resulting value of request handling.
     *
     * @type {boolean | string | UserModel.User | ChatModel.Message | UserModel.MatchUser | undefined}
     */
    value?: boolean | string | UserModel.User | ChatModel.Message | UserModel.MatchUser,

    /**
     * Resulting values of request handling.
     *
     * @type {UserModel.MatchUser[] | ChatModel.Overview[] | ChatModel.Message[] | undefined}
     */
    values?: UserModel.MatchUser[] | ChatModel.Overview[] | ChatModel.Message[]
}

/**
 * Represents error encountered while handling request.
 *
 * @interface
 * @index string
 * @type {string | string[] | undefined}
 */
export interface Error
{
    [index: string]: string | string[] | undefined

    /**
     * Type of error
     *
     * @type {"invalid field" | "duplicate key" | "invalid credentials" | "unknown"}
     */
    type: "invalid field" | "duplicate key" | "invalid credentials" | "unknown",

    /**
     * Resulting value of request handling.
     *
     * @type {string | undefined}
     */
    value?: string,

    /**
     * Resulting values of request handling.
     *
     * @type {string[] | undefined}
     */
    values?: string[]
}
