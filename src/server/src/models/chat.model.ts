/**
 * Represents overview of chat.
 *
 * @interface
 * @index string
 * @type {string | number | Date | null}
 */
export interface Overview
{
  [index: string]: string | number | Date | null

  /**
   * Username of the other user.
   *
   * @type {string}
   */
  chatPartner: string,

  /**
   * Number of unread messages.
   *
   * @type {number}
   */
  unreadMessagesCount: number,

  /**
   * Content of the newest message in chat.
   *
   * @type {string | null}
   */
  latestMessageContent: string | null,

  /**
   * Timestamp of when the newest message in chat was sent.
   *
   * @type {Date | null}
   */
  latestMessageSentAt: Date | null
}

/**
 * Represents chat message.
 *
 * @interface
 * @index string
 * @type {string | Date}
 */
export interface Message
{
  [index: string]: string | Date

  sender: string,
  content: string
  sentAt: Date
}
