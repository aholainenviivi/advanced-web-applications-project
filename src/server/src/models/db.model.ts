import { UUID } from "node:crypto";

/**
 * Represents parameterized database query.
 *
 * @interface
 * @index string
 * @type {string | any[]}
 */
export interface Query
{
    [index: string]: string | any[]

    /**
     * Query string.
     *
     * @type {string}
     */
    text: string,

    /**
     * Query parameters.
     *
     * @type: {any[]}
     */
    values: any[]
}

/**
 * Represent user in database.
 * The optional fields are columns in the users database table that aren't always defined when handling user object outside the database.
 *
 * @interface
 * @index string
 * @type: {string | boolean | UUID | Date | null | undefined}
 */
export interface User
{
    [index: string]: string | boolean | UUID | Date | null | undefined

    user_id?: UUID,
    first_name: string | null,
    last_name: string | null,
    username: string,
    email: string | null,
    bio: string | null,
    password?: string,
    registered_at: Date,
    last_agreed_to_terms_and_conditions?: Date,
    is_admin?: boolean
}

/**
 * Maps variable name in application context to database context.
 *
 * @interface
 * @index string
 * @type {string}
 */
export const FieldMappingUserToDb: { [index: string]: string } = {
    userId: "user_id",
    firstName: "first_name",
    lastName: "last_name",
    username: "username",
    bio: "bio",
    email: "email",
    password: "password",
    registeredAt: "registered_at",
    lastAgreedToTermsAndConditions: "last_agreed_to_terms_and_conditions",
    isAdmin: "is_admin"
}
