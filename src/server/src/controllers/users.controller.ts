import { Request, Response, NextFunction } from "express";
import { matchedData } from "express-validator";
import bcrypt from "bcrypt";

import db from "../db/db";
import { generateToken } from "../auth/jwt";
import { InvalidCredentialsError } from "../errors/errors";
import * as UserModel from "../models/user.model";
import * as ApiModel from "../models/api.model";

/**
 * Middleware that handles user registration.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const register = async(req: Request, res: Response, next: NextFunction): Promise<void> => {
    const data = matchedData(req);

    // construct user object
    const user: UserModel.NewUser = {
        first_name: data.firstName || null,
        last_name: data.lastName || null,
        username: data.username,
        email: data.email || null,
        password: await bcrypt.hash(data.password, 10)
    }

    // add user to db
    await db.Instance.registerUser(user);

    // successful response
    const apiResponse: ApiModel.Response = {
        success: true,
        msg: "User registered."
    };
    res.status(201).json(apiResponse);
}

/**
 * Middleware that handles user log in.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const login = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
   const data = matchedData(req);

   // fetch user from database
   const user: UserModel.User = (await db.Instance.getLoginInfo(data.username))[0];

   // check if user exists
   if (!user)
   {
       next(new InvalidCredentialsError("Invalid login credentials."));
       return;
   }

   // check if password is correct
   const isMatch = await bcrypt.compare(data.password, user.password as string);
   if (!isMatch)
   {
       next(new InvalidCredentialsError("Invalid login credentials."));
       return;
   }

    // generate token
    const token = generateToken(user.userId as string);

    // successful response
    const apiResponse: ApiModel.Response = {
        success: true,
        msg: "User logged in.",
        value: token
    };
    res.status(200).json(apiResponse);
}

/**
 * Middleware for fetching user profile.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const getUserProfile = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const userId = (req.user as { user_id: string }).user_id;

    // fetch profile from db
    const profile: UserModel.User = (await db.Instance.getUserProfile(userId))[0];

    // successful response
    const apiResponse: ApiModel.Response = {
        success: true,
        msg: "User profile fetched.",
        value: profile
    };
    res.status(200).json(apiResponse);
}

/**
 * Middleware for updating user profile.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const updateUserProfile = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const data = matchedData(req);
    const userId = (req.user as { user_id: string }).user_id;
    const fields = Object.keys(data);

    // hash password if changed
    if (fields.includes("password"))
    {
        data.password = await bcrypt.hash(data.password, 10);
    }

    // update user profile in db
    await db.Instance.updateUserProfile(userId, data);

    // successful update
    const apiResponse: ApiModel.Response = {
        success: true,
        msg: "Profile updated."
    };
    res.status(200).json(apiResponse);
}

/**
 * Middleware that fetches profiles of other users.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const getMatchProfiles = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const userId = (req.user as { user_id: string }).user_id;

    // fetch 10 profiles from db
    const profiles: UserModel.MatchUser[] = await db.Instance.getMatchProfiles(userId);

    // successful update
    const ApiResponse: ApiModel.Response = {
        success: true,
        msg: "Profiles fetched.",
        values: profiles
    };
    res.status(200).json(ApiResponse);
}

/**
 * Middleware that handles profile of specified other user.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const getMatchProfile = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const userId = (req.user as { user_id: string }).user_id;
    const data = matchedData(req);

    // fetch profile from db
    const profile: UserModel.MatchUser = (await db.Instance.getMatchProfile(userId, data.username))[0];

    // successful response
    const apiResponse: ApiModel.Response = {
        success: true,
        msg: "User profile fetched.",
        value: profile
    };
    res.status(200).json(apiResponse);
}

/**
 * Middleware that handles deleting user profile.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const deleteUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const userId = (req.user as { user_id: string }).user_id;

    await db.Instance.deleteUser(userId);

    // successful response
    const apiResponse: ApiModel.Response = {
        success: true,
        msg: "User deleted."
    }
    res.status(200).json(apiResponse);
}
