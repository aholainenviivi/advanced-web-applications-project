import { Request, Response, NextFunction } from "express";
import { matchedData } from "express-validator";

import db from "../db/db";
import * as ApiModel from "../models/api.model";
import * as ChatModel from "../models/chat.model";

/**
 * POST /api/chats
 * Middleware that starts a new chat between two users.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns import asyncHandler from "express-async-handler";
 */
export const startChat = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const userId = (req.user as { user_id: string }).user_id;
    const data = matchedData(req);

    // Add new chat to db
    await db.Instance.addChat(userId, data.username);

    const apiResponse: ApiModel.Response = { success: true, msg: "Chat started." };
    res.status(200).json(apiResponse);
}

/**
 * Middleware that gets chats the user is part of.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const getChats = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const userId = (req.user as { user_id: string }).user_id;

    // get chats from db
    const chats: ChatModel.Overview[] = await db.Instance.getChats(userId);

    const apiResponse: ApiModel.Response = {
        success: true,
        msg: "Chats fetched from database.",
        values: chats
    };
    res.status(200).json(apiResponse);
}

/**
 * Middleware that gets all messages of specified chat.
 *
 * @async
 * @function
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const getMessages = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const userId = (req.user as { user_id: string }).user_id;
    const data = matchedData(req);

    // get chat messages from db
    const result: ChatModel.Message[] = await db.Instance.getMessages(userId, data.username);

    // mark messages sent from chat partner as read
    await db.Instance.updateMessagesReadStatus(userId, data.username);

    const apiResponse: ApiModel.Response = {
        success: true,
        msg: "Messages fetched from database.",
        values: result
    }
    res.status(200).json(apiResponse);
}

/**
 * Middleware that gets unread messages that user has received of specified chat.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const getNewMessages = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const userId = (req.user as { user_id: string }).user_id;
    const data = matchedData(req);

    // get new messages from chat partner
    const messages: ChatModel.Message[] = await db.Instance.getNewMessages(userId, data.username);

    // mark messages sent from chat partner as read
    await db.Instance.updateMessagesReadStatus(userId, data.username);

    const apiResponse: ApiModel.Response = {
        success: true,
        msg: "Messages fetched from database.",
        values: messages
    }
    res.status(200).json(apiResponse);
}

/**
 * Middleware that sends the message.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const sendMessage = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const userId = (req.user as { user_id: string }).user_id;
    const data = matchedData(req);

    // upload the message to db
    const message: ChatModel.Message = (await db.Instance.sendMessage(userId, data.username, data.content))[0];

    const apiResponse: ApiModel.Response = {
        success: true,
        msg: "Message sent.",
        value: message
    };
    res.status(200).json(apiResponse);
}
