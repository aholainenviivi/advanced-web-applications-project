import { Request, Response, NextFunction } from "express";
import { matchedData } from "express-validator";

import db from "../db/db";
import * as ApiModel from "../models/api.model";

/**
 * Middleware that handles rejecting user.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const reject = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const userId = (req.user as { user_id: string }).user_id;
    const data = matchedData(req);

    // save rejection to db
    await db.Instance.saveRejection(userId, data.username);

    const apiResponse: ApiModel.Response = {
        success: true,
        msg: "Rejection saved."
    }
    res.status(200).json(apiResponse);
}

/**
 * Middleware that handles liking of user.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const like = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const userId = (req.user as { user_id: string }).user_id;
    const data = matchedData(req);

    // save like to db
    const matchFound = (await db.Instance.saveLike(userId, data.username))[0].count === "1";

    // check if match was found
    if (matchFound)
    {
        // save match to db
        await db.Instance.saveMatch(userId, data.username);
    }

    const apiResponse: ApiModel.Response = {
        success: true,
        msg: "Like saved.",
        value: matchFound
    }
    res.status(200).json(apiResponse);
    return;
}

/**
 * Middleware that handles blocking of user.
 *
 * @function
 * @async
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise<void>}
 */
export const block = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const userId = (req.user as { user_id: string }).user_id;
    const data = matchedData(req);

    // save block to database
    await db.Instance.saveBlock(userId, data.username);

    const apiResponse: ApiModel.Response = {
        success: true,
        msg: "Block saved."
    };
    res.status(200).json(apiResponse);
}
