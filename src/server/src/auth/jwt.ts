import jwt from "jsonwebtoken";

/**
 * Generates JWT token.
 *
 * @function
 * @param {string} id
 * @returns {string}
 */
export const generateToken = (id: string): string => {
    return jwt.sign({ id: id }, process.env.JWT_SECRET as string, { expiresIn: "1 day" });
}
