import { Strategy, ExtractJwt } from "passport-jwt";
import passport from "passport";

import db from "../db/db";
import * as UserModel from "../models/user.model";

/**
 * Configures passport.
 *
 * @function
 * @async
 * @returns {Promise<void>}
 */
export const configure = async (): Promise<void> => {
    const opts = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET as string
    }
    passport.use(new Strategy(opts, (jwtPayload, done) => {
        db.Instance.authQuery(jwtPayload.id)
            .then((result: UserModel.User[]) => {
                if (result === undefined || result.length === 0)
                {
                    return done(null, false);
                }
                else
                {
                    return done(null, result[0]);
                }
            });
    }));
}
