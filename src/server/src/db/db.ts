import { Pool } from "pg";

import { DuplicateKeyConstraintViolationError, UnknownError } from "../errors/errors";
import * as DbModel from "../models/db.model";
import * as UserModel from "../models/user.model";
import * as ChatModel from "../models/chat.model";

/**
 * Represents a database.
 *
 * @class
 */
class DB {
    private static _instance: DB;
    /**
     * Pool of database clients.
     *
     * @type {Pool}
     * @private
     * @readonly
     */
    private readonly _pool: Pool;

    /**
     * Creates an instance of DB.
     *
     * @constructor
     * @private
     */
    private constructor()
    {
        this._pool = new Pool({
            user: process.env.DB_USER,
            database: process.env.DB_NAME,
            password: process.env.DB_PASSWORD,
        });
    }

    /**
     * Instance of the DB class.
     *
     * @getter
     * @static
     */
    static get Instance()
    {
        return this._instance || (this._instance = new this());
    }

    /**
     * Connects to database.
     *
     * @method
     * @returns {void}
     */
    connect = (): void => {
        this._pool.connect()
            .then(() => {
                console.debug("Db connection established");
            })
            .catch((err) => {
                switch (err.code)
                {
                    // "error: role ... does not exist"
                    case ("28000"):
                        console.error(`Db user does not exist. 
                        Please make sure the "bootleg_tinder_admin" database user exists.`);
                        process.exit(1);

                    // "error: database "..." does not exist"
                    case ("3D000"):
                        console.error(`Database does not exist. 
                        Please make sure you've recovered the database correctly or create the "bootleg_tinder" database manually.`);
                        process.exit(1);

                    default:
                        console.error(err);
                        process.exit(1);
                }
            });
    }

    /**
     * Fetches user from database.
     *
     * @method
     * @param {string} userId
     * @returns {Promise<UserModel.User[]>}
     */
    authQuery(userId: string): Promise<UserModel.User[]>
    {
        const query: DbModel.Query = {
            text: `SELECT * 
                   FROM users 
                   WHERE user_id=$1`,
            values: [userId]
        };
        return this._queryDb(query);
    }

    /**
     * Uploads new user to database.
     *
     * @method
     * @param {string} user
     * @returns {Promise<void[]>}
     */
    registerUser(user: UserModel.NewUser): Promise<void[]>
    {
        const query: DbModel.Query = {
            text: `INSERT INTO users(${[...Object.keys(user)]}) 
                   VALUES (${[...Object.keys(user).map(key => `$${Object.keys(user).indexOf(key)+1}`)]})`,
            values: Object.values(user)
        };
        return this._queryDb(query);
    }

    /**
     * Fetches username and password hash from database.
     *
     * @method
     * @param {string} username
     * @returns {Promise<UserModel.User[]>}
     */
    getLoginInfo(username: string): Promise<UserModel.User[]>
    {
        const query: DbModel.Query = {
            text: `SELECT user_id AS "userId", 
                          password 
                   FROM users 
                   WHERE username=$1`,
            values: [username]
        };
        return this._queryDb(query);
    }

    /**
     * Fetches user profile from database.
     *
     * @method
     * @param {string} userId
     * @returns {Promise<UserModel.User[]>}
     */
    getUserProfile(userId: string): Promise<UserModel.User[]>
    {
        const query: DbModel.Query = {
            text: `SELECT first_name AS "firstName",
                          last_name AS "lastName",
                          username,
                          bio,
                          email,
                          registered_at AS "registeredAt"
                   FROM users 
                   WHERE user_id=$1`,
            values: [userId]
        };
        return this._queryDb(query);
    }

    /**
     * Updates user profile in database.
     *
     * @method
     * @param {string} userId
     * @param {string} updatedInfo
     * @returns {Promise<void[]>}
     */
    updateUserProfile(userId: string, updatedInfo: Record<string, any>): Promise<void[]>
    {
        const fields = Object.keys(updatedInfo);
        const query: DbModel.Query = {
            text: "UPDATE users SET ",
            values: []
        };
        let i = 1;
        for (let field of fields)
        {
            query.text += `${DbModel.FieldMappingUserToDb[field]}=$${i++}`;
            query.text += fields.indexOf(field) !== fields.length - 1 ? "," : " ";
            query.values.push(updatedInfo[field]);
        }
        query.text += `WHERE user_id=$${i}`;
        query.values.push(userId);

        return this._queryDb(query);
    }

    /**
     * Fetches the requested profile from database if users have matched.
     *
     * @method
     * @param {string} user1Id
     * @param {string} user2Username
     * @returns {Promise<UserModel.MatchUser[]>}
     */
    getMatchProfile(user1Id: string, user2Username: string): Promise<UserModel.MatchUser[]>
    {
        const query: DbModel.Query = {
            text: `SELECT user2.first_name AS "firstName", 
                          user2.last_name AS "lastName",
                          user2.username,
                          user2.bio
                   FROM users user1
                   JOIN users user2 
                        ON user2.username = $2
                   INNER JOIN matches 
                        ON ((matches.user1 = user1.user_id OR matches.user2 = user1.user_id)
                            AND (matches.user1 = user2.user_id OR matches.user2 = user2.user_id))
                    WHERE user1.user_id = $1
                        AND LEAST(matches.user1, matches.user2) = LEAST(user1.user_id, user2.user_id)
                        AND GREATEST(matches.user1, matches.user2) = GREATEST(user1.user_id, user2.user_id);`,
            values: [user1Id, user2Username]
        };
        return this._queryDb(query);
    }

    /**
     * Fetches 10 random match profiles from database that the user hasn't liked, rejected or blocked, that haven't blocked the user, and that aren't admins.
     *
     * @method
     * @param {string} userId
     * @returns {Promise<UserModel.MatchUser[]>}
     */
    getMatchProfiles(userId: string): Promise<UserModel.MatchUser[]>
    {
        const query: DbModel.Query = {
            text: `SELECT u1.first_name AS "firstName",
                          u1.last_name AS "lastName",
                          u1.username,
                          u1.bio
                   FROM users u1
                   LEFT JOIN users u2
                       ON u2.user_id = $1
                   LEFT JOIN likes
                       ON (likes.liker_user_id = u2.user_id AND likes.liked_user_id = u1.user_id)
                   LEFT JOIN rejections
                       ON (rejections.rejecter_user_id = u2.user_id AND rejections.rejected_user_id = u1.user_id)
                   LEFT JOIN blocks
                       ON ((blocker_user_id = u2.user_id AND blocked_user_id = u1.user_id )
            OR (blocker_user_id = u1.user_id AND blocked_user_id = u2.user_id))
                   WHERE u1.user_id != u2.user_id 
                       AND u1.is_admin = false
                       AND likes.liked_user_id IS NULL
                       AND rejections.rejected_user_id IS NULL
                       AND (blocks.blocker_user_id IS NULL OR blocks.blocked_user_id IS NULL)
                   ORDER BY RANDOM()
                   LIMIT 10;`,
            values: [userId]
        };
        return this._queryDb(query);
    }

    /**
     * Deletes user from database.
     *
     * @method
     * @param {string} userId
     * @returns {Promise<void>}
     */
    deleteUser(userId: string): Promise<void[]>
    {
        const query: DbModel.Query = {
            text: `DELETE FROM users
                   WHERE user_id=$1;`,
            values: [userId]
        };
        return this._queryDb(query);
    }

    /**
     * Saves rejection to database.
     *
     * @method
     * @param {string} rejecterId
     * @param {string} rejectedUsername
     * @returns {Promise<void[]>}
     */
    saveRejection(rejecterId: string, rejectedUsername: string): Promise<void[]>
    {
        const query: DbModel.Query = {
            text: `INSERT INTO rejections(rejecter_user_id, rejected_user_id) 
                   VALUES ($1, (SELECT user_id FROM users WHERE username=$2));`,
            values: [rejecterId, rejectedUsername]
        };
        return this._queryDb(query);
    }

    /**
     * Saves like to database. If the other user has already liked the user, return count of rows of likes in other direction (0 or 1).
     *
     * @method
     * @param {string} likerId
     * @param {string} likedUsername
     * @returns {Promise<{ count: "0" | "1" }[]>}
     */
    saveLike(likerId: string, likedUsername: string): Promise<{ count: "0" | "1" }[]>
    {
        const query: DbModel.Query = {
            text: `INSERT INTO likes(liker_user_id, liked_user_id)
                   VALUES ($1, (SELECT user_id
                                FROM users
                                WHERE username=$2))
                   RETURNING (SELECT COUNT(liked_user_id)
                              FROM likes
                              WHERE liker_user_id=(SELECT user_id FROM users WHERE username=$2)
                                  AND liked_user_id=$1)`,
            values: [likerId, likedUsername]
        };
        return this._queryDb(query)
    }

    /**
     * Saves block to database. Inserting on blocks table triggers a function that deletes likes between users which cascades through all other connections between the users.
     *
     * @method
     * @param {string} blockerId
     * @param {string} blockerUsername
     * @returns {Promise<void[]>}
     */
    saveBlock(blockerId: string, blockerUsername: string): Promise<any[]>
    {
        const query: DbModel.Query = {
            text: `INSERT INTO blocks(blocker_user_id, blocked_user_id)
                   VALUES ($1, (SELECT user_id FROM users WHERE username=$2));`,
            values: [blockerId, blockerUsername]
        };
        return this._queryDb(query);
    }

    /**
     * Saves match to database.
     *
     * @method
     * @param {string} user1Id
     * @param {string} user2Username
     * @returns {Promise<void[]>}
     */
    saveMatch(user1Id: string, user2Username: string): Promise<void[]>
    {
        const query: DbModel.Query = {
            text: `INSERT INTO matches(user1, user2) 
                   VALUES ($1, (SELECT user_id FROM users WHERE username=$2))`,
            values: [user1Id, user2Username]
        };
        return this._queryDb(query);
    }

    /**
     * Uploads new chat to database.
     *
     * @method
     * @param {string} user1Id
     * @param {string} user2Username
     * @returns {Promise<void[]>}
     */
    addChat(user1Id: string, user2Username: string): Promise<void[]>
    {
        const query: DbModel.Query = {
            text: `INSERT INTO chats(user1_id, user2_id, match_id)
                   VALUES ($1,
                           (SELECT user_id FROM users WHERE username=$2),
                           (SELECT match_id
                            FROM matches
                            WHERE LEAST(user1, user2) = LEAST($1, (SELECT user_id FROM users WHERE username=$2))
                              AND GREATEST(user1, user2) = GREATEST($1, (SELECT user_id FROM users WHERE username=$2))));`,
            values: [user1Id, user2Username]
        }
        return this._queryDb(query);
    }

    /**
     * Gets chats that the user is part of from database.
     *
     * @method
     * @param {string} userId
     * @returns {Promise<ChatModel.Overview[]>}
     */
    getChats(userId: string): Promise<ChatModel.Overview[]>
    {
        const query: DbModel.Query = {
            text: `SELECT users.username AS "chatPartner",
                          COALESCE(COUNT(chat_messages.chat_id), 0) AS "unreadMessagesCount",
                          latest_messages.latest_message_content AS "latestMessageContent",
                          latest_messages.latest_message_sent_at AS "latestMessageSentAt"
                   FROM chats
                   JOIN users
                       ON users.user_id = chats.user1_id 
                       OR users.user_id = chats.user2_id
                   LEFT JOIN chat_messages
                       ON chat_messages.chat_id = chats.chat_id
                       AND chat_messages.is_read = FALSE
                       AND chat_messages.user_id != $1
                   LEFT JOIN (
                       SELECT chat_id,
                              content AS latest_message_content,
                              sent_at AS latest_message_sent_at
                       FROM (
                            SELECT chat_id,
                                   content,
                                   sent_at,
                            ROW_NUMBER() OVER (PARTITION BY chat_id ORDER BY sent_at DESC) AS row_num
                       FROM
                            chat_messages
                       ) AS ranked_messages
                       WHERE
                           row_num = 1
                   ) AS latest_messages 
                       ON chats.chat_id = latest_messages.chat_id
                   WHERE (chats.user1_id = $1 OR chats.user2_id = $1)
                       AND users.user_id != $1
                   GROUP BY users.username, latest_messages.latest_message_content, latest_messages.latest_message_sent_at
                   ORDER BY "latestMessageSentAt" DESC;`,
            values: [userId]
        };
        return this._queryDb(query);
    }

    /**
     * Update is_read status to true on all messages in the chat received by the user.
     *
     * @method
     * @param {string} receiverId
     * @param {string} senderUsername
     * @returns {Promise<void[]>}
     */
    updateMessagesReadStatus(receiverId: string, senderUsername: string): Promise<void[]>
    {
        const query: DbModel.Query = {
            text: `UPDATE chat_messages
                SET is_read = true
                WHERE chat_id = (SELECT chat_id 
                                 FROM chats 
                                 WHERE LEAST(user1_id, user2_id) = LEAST($1, (SELECT user_id FROM users WHERE username=$2))
                                   AND GREATEST(user1_id, user2_id) = GREATEST($1, (SELECT user_id FROM users WHERE username=$2))) 
                  AND user_id = (SELECT user_id 
                                 FROM users
                                 WHERE username=$2);`,
            values: [receiverId, senderUsername]
        };
        return this._queryDb(query);
    }

    /**
     * Gets messages of the requested chat from the database.
     *
     * @method
     * @param {string} receiverId
     * @param {string} senderUsername
     * @returns {Promise<ChatModel.Message[]>}
     */
    getMessages(receiverId: string, senderUsername: string): Promise<ChatModel.Message[]>
    {
        const query: DbModel.Query = {
            text: `SELECT users.username AS sender,
                    chat_messages.content,
                    chat_messages.sent_at AS "sentAt"
                FROM chats
                JOIN chat_messages
                    ON chat_messages.chat_id = chats.chat_id
                INNER JOIN users
                    ON chat_messages.user_id = users.user_id
                WHERE (chat_messages.user_id = $1
                    OR chat_messages.user_id = (SELECT users.user_id FROM users WHERE username=$2))
                    AND LEAST(chats.user1_id, chats.user2_id) = LEAST($1, (SELECT users.user_id FROM users WHERE username=$2))
                    AND GREATEST(chats.user1_id, chats.user2_id) = GREATEST($1, (SELECT users.user_id FROM users WHERE username=$2))
                ORDER BY sent_at;`,
            values: [receiverId, senderUsername]
        };
        return this._queryDb(query);
    }

    /**
     * Gets unread messages of the requested chat from database.
     *
     * @method
     * @param {string} receiverId
     * @param {string} senderUsername
     * @returns {Promise<ChatModel.Message[]>}
     */
    getNewMessages(receiverId: string, senderUsername: string): Promise<ChatModel.Message[]> {
        const query: DbModel.Query = {
            text: `SELECT users.username AS sender,
                      chat_messages.content,
                      chat_messages.sent_at AS "sentAt"
                FROM chats
                JOIN chat_messages
                    ON chat_messages.chat_id = chats.chat_id
                INNER JOIN users
                    ON chat_messages.user_id = users.user_id
                WHERE (chat_messages.user_id = (SELECT users.user_id FROM users WHERE username=$2))
                  AND LEAST(chats.user1_id, chats.user2_id) = LEAST($1, (SELECT users.user_id FROM users WHERE username=$2))
                  AND GREATEST(chats.user1_id, chats.user2_id) = GREATEST($1, (SELECT users.user_id FROM users WHERE username=$2))
                  AND chat_messages.is_read = FALSE
                ORDER BY sent_at;`,
            values: [receiverId, senderUsername]
        }
        return this._queryDb(query);
    }

    /**
     * Uploads the message to database and returns the message from database.
     *
     * @method
     * @param {string} senderId
     * @param {string} receiverUsername
     * @param {string} content
     * @returns {Promise<ChatModel.Message[]>}
     */
    sendMessage(senderId: string, receiverUsername: string, content: string): Promise<ChatModel.Message[]> {
        const query: DbModel.Query = {
            text: `INSERT INTO chat_messages(user_id, chat_id, content)
                VALUES ($1,
                        (SELECT chat_id
                         FROM chats
                         WHERE LEAST(chats.user1_id, chats.user2_id) = LEAST($1, (SELECT users.user_id FROM users WHERE username=$2))
                           AND GREATEST(chats.user1_id, chats.user2_id) = GREATEST($1, (SELECT users.user_id FROM users WHERE username=$2))),
                        $3)
                RETURNING (SELECT username FROM users WHERE users.user_id=chat_messages.user_id) AS sender,
                          content,
                          sent_at AS "sentAt";`,
            values: [senderId, receiverUsername, content]
        };
        return this._queryDb(query);
    }

    /**
     * Queries the database.
     *
     * @param {DbModel.Query} query
     * @returns {Promise<any[]>}
     * @throws {DuplicateKeyConstraintViolationError | UnknownError}
     */
    private _queryDb = async (query: DbModel.Query): Promise<any[]> => {
        try
        {
            const result = await this._pool.query(query.text, query.values);
            return result.rows;
        }
        catch (err: any)
        {
            switch (err.code)
            {
                // error: duplicate key value violates unique constraint
                case "23505":
                    // err.constraint structure = <table_name>_<column_name>_key
                    const value = err.constraint.slice(err.table.length+1, err.constraint.length - 4)
                    throw new DuplicateKeyConstraintViolationError("Duplicate key constraint violation in database operation.", value);

                // "error: relation "..." does not exist"
                case("42P01"):
                    console.error(`Database is missing required table(s).
                     Please make sure you've recovered the database properly or create the tables manually.`);
                    process.exit(1);

                // unknown error
                default:
                    throw new UnknownError("Unknown error occurred in database operation.");
            }
        }
    }
}

export default DB;
