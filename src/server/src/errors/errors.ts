import * as ApiModel from "../models/api.model";

/**
 * Represent custom error.
 *
 * @class
 * @extends {Error}
 */
export class CustomError extends Error
{
    /**
     * HTTP status code associated with the error.
     *
     * @type {number}
     */
    statusCode!: number;
    /**
     * Response sent to client after the error.
     *
     * @type {ApiModel.Response}
     */
    response!: ApiModel.Response;

    /**
     * Creates an instance of CustomError.
     *
     * @constructor
     * @param {string} message
     */
    constructor(message: string)
    {
        super(message);
        Object.setPrototypeOf(this, CustomError.prototype);
    }
}

/**
 * Represent custom error thrown when request contains invalid field(s).
 *
 * @class
 * @extends {Error}
 */
export class InvalidRequestFieldError extends CustomError
{
    statusCode = 422;

    /**
     * Creates an instance of InvalidRequestFieldError.
     *
     * @constructor
     * @param {string} message
     * @param {string[]} values
     */
    constructor(message: string, values: string[])
    {
        super(message);
        this.response = {
            success: false,
            msg: "Invalid field(s) in request.",
            error: {
                type: "invalid field",
                values: values
            }
        };
    }

}

/**
 * Represent custom error thrown in cases of duplicate key constraint violation.
 *
 * @class
 * @extends {Error}
 */
export class DuplicateKeyConstraintViolationError extends CustomError
{
    statusCode = 409;

    /**
     * Creates an instance of DuplicateKeyConstraintViolationError.
     *
     * @constructor
     * @param {string} message
     * @param {string} value
     */
    constructor(message: string, value: string)
    {
        super(message);
        this.response = {
            success: false,
            msg: "Duplicate key constraint violation.",
            error: {
                type: "duplicate key",
                value: value
            }
        };
    }
}

/**
 * Represent custom error thrown when invalid credentials are used.
 *
 * @class
 * @extends {Error}
 */
export class InvalidCredentialsError extends CustomError
{
    statusCode = 401;

    /**
     * Creates an instance of InvalidCredentialsError.
     *
     * @constructor
     * @param {string} message
     */
    constructor(message: string)
    {
        super(message);
        this.response = {
            success: false,
            msg: "Invalid credentials",
            error: {
                type: "invalid credentials"
            }
        };
    }
}

/**
 * Represent custom error thrown when error type is unknown.
 *
 * @class
 * @extends {Error}
 */
export class UnknownError extends CustomError
{
    statusCode = 500;

    /**
     * Creates an instance of UnknownError.
     *
     * @constructor
     * @param {string} message
     */
    constructor(message: string)
    {
        super(message);
        this.response = {
            success: false,
            msg: "An unknown error occurred.",
            error: {
                type: "unknown"
            }
        };
    }
}
